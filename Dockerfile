#Imagen base (Lo tomas desde el ultima imagen node)
FROM node:latest

#Directorio de a APP
WORKDIR /app

#copio los archivos
ADD /build/default /app/build/default
ADD server.js /app
ADD package.json /app

#Dependencias  (TOmarlas cuando descargo la imagen)
RUN npm install

#Puerto que se expone
EXPOSE 3000

#Comando de ejecucion
CMD ["npm","start"]
